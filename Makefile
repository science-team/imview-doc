
INSTALL=/usr/bin/install

install:
	${INSTALL} -d $(DESTDIR)/usr/share/doc/imview-doc
	cp imview.pdf $(DESTDIR)/usr/share/doc/imview-doc
	${INSTALL} -d $(DESTDIR)/usr/share/doc/imview-doc/html
	cp html/* $(DESTDIR)/usr/share/doc/imview-doc/html
clean:
	rm -rf $(DESTDIR)
